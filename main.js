window.onload = (event) =>{
  const battle_music = new Audio("pokemon_battle.mp3");
  battle_music.volume = 0.025;
  const route_music = new Audio("pokemon_route.mp3");
  route_music.volume = 0.075;
  route_music.play();
  const sad_music = new Audio("pokemon_emotion.mp3");
  sad_music.volume = 0.25;


  const BASE_URL = "https://pokeapi.co/api/v2/";

  const scene = document.getElementById("js--scene")

  const camera = document.getElementById("js--camera");
  const rig = document.getElementById("js--rig");
  const cursor = document.getElementById("js--cursor");

  const ui = document.getElementById("js--ui");
  const ui__name = document.getElementById("js--ui__name");

  const dialogueParent = document.getElementById("js--dialogueParent");
  dialogueParent.setAttribute("visible", "false");
  const dialogue = document.getElementById("js--dialogue");

  const intro = document.getElementById("js--intro");

  let buttonsUI = [];
  let buttonsUI_text = [];

  const pokemonNames = document.getElementsByClassName("js--pokemonName");
  const pokemonImages = document.getElementsByClassName("js--pokemonImage");
  const pokemonSelects = document.getElementsByClassName("js--pokemonSelect");
  let pokeball = document.getElementById("js--pokeball");
  const startPokemon = ["charmander", "bulbasaur", "squirtle"];

  const directions = document.getElementsByClassName("js--directionArrow");

  const turnables = document.getElementsByClassName("turnable");

  const pokeballTemplate = pokeball.cloneNode().setAttribute("static-body", "");

  const worldpoint = new CANNON.Vec3(0,0,0);

  let wildPokemons = [];

  let lastCaughtPokemon = "";

  const attackAnimation = "property: scale; to: 2.5 1.5 2.5; dur: 500; loop: 2; dir: alternate;";

  const pokemonInfoTemplate = document.getElementById("js--pokemonInfoTemplate").cloneNode("true");
  document.getElementById("js--pokemonInfoTemplate").remove();

  //STATE
  let selectedPokemon = "";
  let selectedPokemonSRC = "";
  let selectedPokemonBackSRC = "";
  let selectPokemonMoves = [];

  let numOfCaughtPokemon = 0;

  let allowedToMove = false;

  let inCombat = false;

  let equippedPokeball = false;
  let readiedPokeball = false;

  //CONFIG
  let numberOfPokemon = 50;
  let numberOfShinies = numberOfPokemon/10;

  let maxSpawnRangeOfPokemon = 20;
  let minSpawnRangeOfPokemon = 5;

  const throwForceMultiplier = 6000;
  const verticalThrowMultipler = 10;

  const minimumEncounterDistance = 5;

  const fightingDistance = 5;

  let health = 200;

  //generate starter pokemon
  for (let i = 0; i < pokemonNames.length; i++) {
    fetch(BASE_URL + "pokemon/" + startPokemon[i])
    .then( (data) =>{
      return data.json();
    })
    .then( (response) =>{
      pokemonNames[i].setAttribute("value", response.name);
      pokemonImages[i].setAttribute("src", response.sprites.front_default);
      pokemonImages[i].setAttribute("data-backsrc", response.sprites.back_default);

      pokemonImages[i].setAttribute("data-move0", response.moves[0].move.name);
      pokemonImages[i].setAttribute("data-move1", response.moves[2].move.name);
    })
  }

  //select start pokemon
  for (let i = 0; i < pokemonSelects.length; i++) {
    pokemonSelects[i].onclick = (event) =>{
      if(selectedPokemon !== ""){return;}
      //set data to var
      selectedPokemon = pokemonNames[i].getAttribute("value");
      selectedPokemonSRC = pokemonSelects[i].getAttribute("src");
      selectedPokemonBackSRC = pokemonImages[i].getAttribute("data-backsrc");
      selectPokemonMoves.push(pokemonSelects[i].getAttribute("data-move0"), pokemonSelects[i].getAttribute("data-move1"));

      pokeball.setAttribute("animation__selected", "property: scale; dur:2000; to: 0.35 0.35 0.35;");
      pokemonImages[i].setAttribute("animation__moveDown", "property: position; dur:2000; to: 0 1 -3");

      let timeHandler = setTimeout(function(){
        //set pokemon as selected in ui
        ui__name.setAttribute("src", pokemonSelects[i].getAttribute("src"));
        //remove intro
        intro.remove();
        pokeball.setAttribute("dynamic-body", "");
        pokeball.classList.add("clickable");

      },2500)

    }
  }


  //update caught pokemon UI
  const caughtPokemon = (pokemon) =>{
    if (pokemon.parentNode.getAttribute("data-pokemonname") === lastCaughtPokemon) {return;}

    lastCaughtPokemon = pokemon.parentNode.getAttribute("data-pokemonname");

    console.log("caught pokemon: " + pokemon.parentNode.getAttribute("data-pokemonname"));

    let caughtPokemonImage = document.createElement("a-image");

    caughtPokemonImage.setAttribute("src", pokemon.getAttribute("src"));
    caughtPokemonImage.setAttribute("material","opacity:0.4");
    caughtPokemonImage.setAttribute("position", "1.5 " + (0.65 - (0.15 * numOfCaughtPokemon)) + " -1");
    caughtPokemonImage.setAttribute("scale", "0.2 0.2 0.2");


    camera.appendChild(caughtPokemonImage);

    numOfCaughtPokemon++;
  }



  //pick up pokeball
const updatePickup = () =>{
  pokeball.onclick = (event) =>{
    if (equippedPokeball == true) {//pickup from belt to camera, ready to throw

      readiedPokeball = true;
      equippedPokeball = false;

      let pokeballClone = pokeball.cloneNode();
      camera.appendChild(pokeballClone);
      pokeballClone.setAttribute("position", "1.3 -0.5 -1");
      pokeballClone.removeAttribute("dynamic-body");

      pokeball.remove();
      pokeball = pokeballClone;

      updatePickup();
      updateCollision();

    }else{//pickup from ground
      let pokeballCopy = pokeball.cloneNode();
      rig.appendChild(pokeballCopy);
      pokeballCopy.setAttribute("position", "-0.5 1 -0.5");
      pokeballCopy.removeAttribute("dynamic-body");

      pokeball.remove();
      equippedPokeball = true;
      pokeball = document.getElementById("js--pokeball");

      //enable HUD
      ui.setAttribute("visible", "true");

      //allow to move:
      allowedToMove = true;
      for (let i = 0; i < directions.length; i++) {
        directions[i].setAttribute("visible", "true");
      }

      //update pickup onclick
      updatePickup();
      updateCollision();
    }

  }
}
updatePickup();


  const throwPokeball = () => {
    //get & parse rotation to clean 0-360 degrees
    let horizontalDegrees = (((camera.getAttribute("rotation").y % 360) + 360) % 360).toFixed();
    let horizontalRadians = horizontalDegrees * (Math.PI / 180);

    let verticalDegrees = Math.min(Math.max(parseInt(camera.getAttribute("rotation").x), 1), 90);

    let throwForce = new CANNON.Vec3(Math.sin(horizontalRadians) * -throwForceMultiplier,
                                    verticalDegrees * ((throwForceMultiplier / verticalThrowMultipler) + 2000),
                                    Math.cos(horizontalRadians) * -throwForceMultiplier);

    // let worldpoint = new CANNON.Vec3(pokeball.getAttribute("position").x,
    //                                 pokeball.getAttribute("position").y,
    //                                 pokeball.getAttribute("position").z,);

    pokeball.body.applyForce(throwForce, worldpoint);
  }

  const updateCollision = () =>{
    pokeball.addEventListener("collide", (event)=>{
      let collider = event.detail.body.el;
      if (collider.getAttribute("id") !== "js--resetPlane") {

        let catchChance = Math.ceil((collider.parentNode.getAttribute("data-health")) / 10);
        let catchRNG = Math.floor(Math.random()*catchChance) + 1

        if (true) {
          console.log("hit");
          dialogue.setAttribute("value", "You've caught a " + collider.parentNode.getAttribute("data-pokemonname") + "!");
          dialogueParent.setAttribute("visible", "true");

          caughtPokemon(collider);

          setTimeout(()=>{
            collider.parentNode.remove();
          },1);

          setTimeout(()=>{
            dialogueParent.setAttribute("visible", "false");
          }, 2000)
        }
      }else{
        //reset

      }
    })
  }
  updateCollision();


  //spawn pokemon
  for (let i = 0; i < numberOfPokemon; i++) {
    let randomPokemonNumber = Math.floor(Math.random()*151)+1;

    let randomRadiant = (Math.floor(Math.random() * 360) + 1) * (Math.PI/180);
    let randomDistance = Math.floor(Math.random() * (maxSpawnRangeOfPokemon - minSpawnRangeOfPokemon)) + (1 + minSpawnRangeOfPokemon);

    let randomPokemonPositionX = Math.floor(Math.cos(randomRadiant) * randomDistance) + 1;
    let randomPokemonPositionZ = Math.floor(Math.sin(randomRadiant) * randomDistance) + 1;

    let randomPokemon = document.createElement("a-entity");
    let randomPokemonImage = document.createElement("a-image");
    randomPokemon.appendChild(randomPokemonImage);
    scene.appendChild(randomPokemon);

    fetch(BASE_URL + "pokemon/" + randomPokemonNumber)
    .then( (data) =>{
      return data.json();
    })
    .then( (response) =>{
      if(numberOfShinies > 0){
        randomPokemonImage.setAttribute("src", response.sprites.front_shiny);
        randomPokemonImage.setAttribute("scale", "2.5 2.5 2.5");
        randomPokemonImage.setAttribute("animation__shiny", "property: color; to: rgb(255,255,255); from: rgb(255,255,0); dur: 1000; dir: alternate; loop:true;");

        numberOfShinies--;

        console.log("spawned shiny: " +  response.name);
      }else{
        randomPokemonImage.setAttribute("src", response.sprites.front_default);
        randomPokemonImage.setAttribute("scale", "2 2 2");
      }

      randomPokemonImage.setAttribute("static-body", "");
      randomPokemonImage.classList.add("turnable", "clickable", "js--pokemon");
      randomPokemon.setAttribute("position", randomPokemonPositionX + " 1 " + randomPokemonPositionZ);
      randomPokemon.setAttribute("data-pokemonName", response.name);
      randomPokemon.setAttribute("data-id", randomPokemonNumber);
      randomPokemon.setAttribute("data-level", Math.floor(Math.random()*40)+1);
      randomPokemon.setAttribute("data-health", 100);

      for (let j = 0; j < 2; j++) {
        randomPokemon.setAttribute("data-move" + j, response.moves[j].move.name);
      }

      randomPokemon.classList.add("js--pokemonParent");

      wildPokemons.push(randomPokemonImage);
    })
  }

  //turn all pokemons towards player
  const facePokemon = () =>{
    for (var j = 0; j < turnables.length; j++) {
      let deltaX = turnables[j].getAttribute("position").x - rig.getAttribute("position").x;
      let deltaZ = turnables[j].getAttribute("position").z - rig.getAttribute("position").z;

      turnables[j].setAttribute("rotation", "0 " + (Math.atan2(deltaZ, deltaX) * 180 / Math.PI) + " 0");

    }
  }
  facePokemon();

  const hitEffect = (target) =>{
    let hurtCount = 0
    let hurtHandler = setInterval(()=>{
      if(hurtCount == 7){
        clearInterval(hurtHandler);
      }else{
        if (hurtCount % 2 === 0) {
          target.setAttribute("material","opacity",1);
        }else{
          target.setAttribute("material","opacity",0.2);
        }
        hurtCount++;
      }
    },200)
  }


  //movement
  for (let i = 0; i < directions.length; i++) {

    //start moving
    directions[i].onmouseenter = (event) =>{
      //only if allowed to move > continue
      if(!allowedToMove){return;}

      //repeat move animation
      let movementHandler = setInterval(()=>{
        //define animation and direction of movement
        let animationString = "property: position; dur: 250; easing: linear; to: "
        let directionThrow = directions[i].getAttribute("data-direction").split(" ");

        //move rig
        rig.setAttribute("animation__moveRig", animationString +
        (rig.getAttribute("position").x + parseInt(directionThrow[0])) + " " +
        (rig.getAttribute("position").y) + " " +
        (rig.getAttribute("position").z + parseInt(directionThrow[2])));

        //rotate sprites to face player
        facePokemon();
      },250);

      //stop moving
      directions[i].onmouseleave = (event) =>{
        clearInterval(movementHandler)
      }

    }
  }


  //setup start
  setTimeout(()=>{
    for (let i = 0; i < wildPokemons.length; i++) {
      //change color of cursor depending of youre in range to start encounter or not
      wildPokemons[i].onmouseenter = (event) =>{
        //dont allow if already in combat
        if (inCombat) {return;}

        let deltaX = wildPokemons[i].parentNode.getAttribute("position").x - rig.getAttribute("position").x;
        let deltaZ = wildPokemons[i].parentNode.getAttribute("position").z - rig.getAttribute("position").z;

        let distance = Math.hypot(deltaZ, deltaX);

        if (distance < minimumEncounterDistance){
          cursor.setAttribute("material", "color", "green");
        }else{
          cursor.setAttribute("material", "color", "red");
        }
      }
      wildPokemons[i].onmouseleave = (event) =>{
        cursor.setAttribute("material", "color", "black");
      }

      wildPokemons[i].onclick = (event) =>{
        //throw pokeball if in hand
        if (readiedPokeball) {
          let pokeballClone = pokeball.cloneNode(true);

          pokeball.remove();

          scene.appendChild(pokeballClone);
          pokeballClone.setAttribute("position", rig.getAttribute("position").x + " 0.5 " + rig.getAttribute("position").z);
          pokeballClone.setAttribute("dynamic-body", "");

          updatePickup();
          updateCollision();

          setTimeout(()=>{
            pokeball = pokeballClone;

            throwPokeball();
            readiedPokeball = false;
            equippedPokeball = false;

            updatePickup();
            updateCollision();
          },100);

        }

        //dont allow if already in combat
        if (inCombat) {return;}

        //calculate distance player to pokemon
        //pythagoras
        let deltaX = wildPokemons[i].parentNode.getAttribute("position").x - rig.getAttribute("position").x;
        let deltaZ = wildPokemons[i].parentNode.getAttribute("position").z - rig.getAttribute("position").z;

        let distance = Math.hypot(deltaZ, deltaX);

        //if player is within var minimumEncounterDistance, start encounter
        if (distance < minimumEncounterDistance) {
          //only start encounter if you have pokemon equipped
          if(selectedPokemon === ""){return;}

          console.log("started encounter with " + wildPokemons[i].parentNode.getAttribute("data-pokemonname"));

          startEncounter(wildPokemons[i]);
        }
      }

    }
  }, 1500);


  const startEncounter = (enemyPokemon) =>{
    route_music.currentTime = 0;
    route_music.pause();
    battle_music.play();

    buttonsUI = [];
    buttonsUI_text = [];
    //set dialogue to: found pokemon
    dialogueParent.setAttribute("visible", "true");
    dialogue.setAttribute("value", "A wild " + enemyPokemon.parentNode.getAttribute("data-pokemonname") + " appeared!");
    //set state to in combat
    inCombat = true;
    //stop player from moving
    allowedToMove = false;
    for (let i = 0; i < directions.length; i++) {
      directions[i].setAttribute("visible", "false");
    }
    //remove clickable from pokemons except current pokemon
    for (let i = 0; i < wildPokemons.length; i++) {
      if(wildPokemons[i] == enemyPokemon){continue;}
      wildPokemons[i].classList.remove("clickable");
    }

    //move rig
    let animationString = "property: position; dur: 2000; easing: linear; to: "
    rig.setAttribute("animation__moveRig2", animationString +
    (enemyPokemon.parentNode.getAttribute("position").x + fightingDistance) + " " +
    (rig.getAttribute("position").y) + " " +
    (enemyPokemon.parentNode.getAttribute("position").z));

    setTimeout(() =>{
      //set dialogue to: what will ... do?
      dialogue.setAttribute("value", "What will " + selectedPokemon + " do?")
      //turn pokemon towards player
      facePokemon();

      let enemyPokemonInfo = pokemonInfoTemplate.cloneNode(true);
      enemyPokemonInfo.removeAttribute("id");

      //setup of enemy
      scene.appendChild(enemyPokemonInfo);

      enemyPokemonInfo.setAttribute("position", enemyPokemon.parentNode.getAttribute("position").x + " 2 " + (enemyPokemon.parentNode.getAttribute("position").z + 2.5));
      enemyPokemonInfo.setAttribute("rotation", "0 90 0");

      enemyPokemonInfo.childNodes[1].setAttribute("value", enemyPokemon.parentNode.getAttribute("data-pokemonname"));
      enemyPokemonInfo.childNodes[3].setAttribute("value", "Lv:" + enemyPokemon.parentNode.getAttribute("data-level"));
      enemyPokemonInfo.childNodes[5].setAttribute("width", (enemyPokemon.parentNode.getAttribute("data-health") / 50));

      //setup own pokemon
      let ownedPokemonImage = document.createElement("a-image");

      scene.appendChild(ownedPokemonImage);

      ownedPokemonImage.setAttribute("src", selectedPokemonBackSRC);
      ownedPokemonImage.setAttribute("scale", "2 2 2");
      ownedPokemonImage.setAttribute("rotation", "0 90 0");
      ownedPokemonImage.setAttribute("position", (rig.getAttribute("position").x - 1) + " 1 " + (rig.getAttribute("position").z - 1));

      //setup fight ui
      let encounterUI = document.createElement("a-box");
      encounterUI.setAttribute("width", "2.5");
      encounterUI.setAttribute("depth", "0.1");
      encounterUI.setAttribute("rotation", "0 135 0");

      let friendlyHealthbar = document.createElement("a-box");
      friendlyHealthbar.setAttribute("height", "0.1");
      friendlyHealthbar.setAttribute("depth", "0.1");
      friendlyHealthbar.setAttribute("position", "0 0.35 0.05");
      friendlyHealthbar.setAttribute("color", "red");
      friendlyHealthbar.setAttribute("width", Math.floor(health / 100).toString());

      let selectMove1 = document.createElement("a-text");
      selectMove1.setAttribute("value", selectPokemonMoves[0]);
      selectMove1.setAttribute("align", "center");
      selectMove1.setAttribute("position", "-0.5 0.15 0.075");
      selectMove1.setAttribute("width", "3");
      buttonsUI_text.push(selectMove1);
      let selectMove1Background = document.createElement("a-plane");
      selectMove1Background.setAttribute("position", "-0.5 0.15 0.06");
      selectMove1Background.setAttribute("width", "1");
      selectMove1Background.setAttribute("height", "0.2");
      selectMove1Background.setAttribute("color", "gray");
      selectMove1Background.classList.add("clickable");
      buttonsUI.push(selectMove1Background)

      let selectMove2 = document.createElement("a-text");
      selectMove2.setAttribute("value", selectPokemonMoves[1]);
      selectMove2.setAttribute("align", "center");
      selectMove2.setAttribute("position", "0.5 0.15 0.075");
      selectMove2.setAttribute("width", "3");
      buttonsUI_text.push(selectMove2);
      let selectMove2Background = document.createElement("a-plane");
      selectMove2Background.setAttribute("position", "0.5 0.15 0.06");
      selectMove2Background.setAttribute("width", "1");
      selectMove2Background.setAttribute("height", "0.2");
      selectMove2Background.setAttribute("color", "gray");
      selectMove2Background.classList.add("clickable");
      buttonsUI.push(selectMove2Background);

      let selectRun = document.createElement("a-text");
      selectRun.setAttribute("value", "run");
      selectRun.setAttribute("position", "0.5 -0.25 0.075");
      selectRun.setAttribute("align", "center");
      selectRun.setAttribute("width", "3")
      let selectRunBackground = document.createElement("a-plane");
      selectRunBackground.setAttribute("position", "0.5 -0.25 0.06");
      selectRunBackground.setAttribute("width", "1");
      selectRunBackground.setAttribute("height", "0.2");
      selectRunBackground.setAttribute("color", "gray");
      selectRunBackground.classList.add("clickable");

      encounterUI.appendChild(friendlyHealthbar);
      encounterUI.appendChild(selectMove1Background);
      encounterUI.appendChild(selectMove1);
      encounterUI.appendChild(selectMove2Background);
      encounterUI.appendChild(selectMove2);
      encounterUI.appendChild(selectRunBackground);
      encounterUI.appendChild(selectRun);

      encounterUI.setAttribute("position", (rig.getAttribute("position").x - 1) + " 1 " + (rig.getAttribute("position").z + 2));
      scene.appendChild(encounterUI);

      for (let k = 0; k < buttonsUI.length; k++) {
        buttonsUI[k].onclick = (event) =>{
          dialogue.setAttribute("value", selectedPokemon + " used " + buttonsUI_text[k].getAttribute("value") + "!");
          ownedPokemonImage.setAttribute("animation__attacking", attackAnimation);

          setTimeout(()=>{
            ownedPokemonImage.removeAttribute("animation__attacking");
            let friendlyRNG = Math.floor(Math.random() * 10);
            if (friendlyRNG <= 1) {//missed attack
              dialogue.setAttribute("value", selectedPokemon + "'s attack missed!");
            }else if(friendlyRNG >= 8){//critical hit
              dialogue.setAttribute("value", "A critical hit for " + (friendlyRNG * 10) + " damage!");
              hitEffect(enemyPokemon);

              enemyPokemon.parentNode.setAttribute("data-health", enemyPokemon.parentNode.getAttribute("data-health") - (friendlyRNG*10));
              enemyPokemonInfo.childNodes[5].setAttribute("width", (enemyPokemon.parentNode.getAttribute("data-health") / 50));
            }else{//default
              dialogue.setAttribute("value", selectedPokemon + "'s attack hit for " + (friendlyRNG * 10) + " damage!");
              hitEffect(enemyPokemon);

              enemyPokemon.parentNode.setAttribute("data-health", enemyPokemon.parentNode.getAttribute("data-health") - (friendlyRNG*10));
              enemyPokemonInfo.childNodes[5].setAttribute("width", (enemyPokemon.parentNode.getAttribute("data-health") / 50));
            }

            if (enemyPokemon.parentNode.getAttribute("data-health") < 1) {//DEATH OF ENEMY POKEMON
              //remove from scene
              encounterUI.remove();
              ownedPokemonImage.remove();
              enemyPokemonInfo.remove();

              //allowed to fight
              inCombat = false;

              //allow to move:
              allowedToMove = true;
              for (let i = 0; i < directions.length; i++) {
                directions[i].setAttribute("visible", "true");
              }

              health = Math.min(Math.max(parseInt(health + 100), 0), 200); //regen  100 health

              //add clickable from pokemons
              for (let i = 0; i < wildPokemons.length; i++) {
                wildPokemons[i].classList.add("clickable");
              }

              dialogue.setAttribute("value", enemyPokemon.parentNode.getAttribute("data-pokemonname") + " has fainted!")

              //remove enemy pokemon
              setTimeout(()=>{
                enemyPokemon.parentNode.remove();
                setTimeout(()=>{//remove dialogue
                  dialogueParent.setAttribute("visible", "false");
                },400)
              },600);

              battle_music.currentTime = 0;
              battle_music.pause();
              route_music.play();
            }

            let enemyRNG = Math.floor(Math.random() * 10);
            let attackRNG = Math.floor(Math.random() * 2);
            let enemyAttack = enemyPokemon.parentNode.getAttribute("data-move" + attackRNG);

            setTimeout(()=>{
              dialogue.setAttribute("value", enemyPokemon.parentNode.getAttribute("data-pokemonname") + " used " + enemyAttack + "!");

              enemyPokemon.setAttribute("animation__attacking", attackAnimation);

              setTimeout(()=>{
                ownedPokemonImage.removeAttribute("animation__attacking");
                if (enemyRNG <= 1) {//missed attack
                  dialogue.setAttribute("value", enemyPokemon.parentNode.getAttribute("data-pokemonname") + "'s attack missed!");
                }else if(friendlyRNG >= 8){//critical hit
                  dialogue.setAttribute("value", "A critical hit for " + (enemyRNG * 10) + " damage!");
                  hitEffect(ownedPokemonImage);

                  health -= enemyRNG * 10;
                  friendlyHealthbar.setAttribute("width", (health / 100));
                }else{//default
                  dialogue.setAttribute("value", enemyPokemon.parentNode.getAttribute("data-pokemonname") + "'s attack hit for " + (enemyRNG * 10) + " damage!");
                  hitEffect(ownedPokemonImage);

                  health -= enemyRNG * 10;
                  friendlyHealthbar.setAttribute("width", (health / 100));
                }

                if (health < 1) { //DEATH OF PLAYER
                  //spawm gravestone
                  let gravestone = document.createElement("a-entity");
                  gravestone.setAttribute("obj-model", "obj:#gravestone.obj; mtl: #gravestone.mtl;");
                  gravestone.setAttribute("position", ((rig.getAttribute("position").x - 2) + " -2.5 " + rig.getAttribute("position").z));
                  gravestone.setAttribute("rotation", "0 90 0");

                  //add rip text
                  let ripText = document.createElement("a-text");
                  ripText.setAttribute("value", "R I P");
                  ripText.setAttribute("position", "0 1.65 .1");
                  ripText.setAttribute("align", "center");
                  gravestone.appendChild(ripText);

                  let retryButton = document.createElement("a-plane");
                  retryButton.setAttribute("position", "0 1.2 .2");
                  retryButton.setAttribute("height", "0.25");
                  retryButton.setAttribute("width", "0.75");
                  retryButton.classList.add("clickable");

                  let retryButtonText = document.createElement("a-text");
                  retryButtonText.setAttribute("value", "retry");
                  retryButtonText.setAttribute("align", "center");
                  retryButtonText.setAttribute("position", "0 0 0.001");
                  retryButton.appendChild(retryButtonText);
                  gravestone.appendChild(retryButton);

                  scene.appendChild(gravestone);
                  gravestone.setAttribute("animation", "property: position; to: " + (rig.getAttribute("position").x - 2) + " -0.15 " + rig.getAttribute("position").z + "; dur: 2000; easing: linear;");

                  //restart on button click
                  retryButton.onclick = () =>{
                    location.reload();
                  }

                  //remove from scene
                  encounterUI.remove();
                  ownedPokemonImage.remove();
                  enemyPokemonInfo.remove();
                  pokeball.remove();
                  dialogueParent.setAttribute("visible", "false");

                  battle_music.currentTime = 0;
                  battle_music.pause();
                  sad_music.play();
                }
              },1000)
            },1500)



          },1000);
        }
      }


      selectRunBackground.onclick = (event) =>{
        dialogue.setAttribute("value", selectedPokemon + " ran!");

        //remove from scene
        encounterUI.remove();
        ownedPokemonImage.remove();
        enemyPokemonInfo.remove();

        //allowed to fight
        inCombat = false;

        health = Math.min(Math.max(parseInt(health + 50), 0), 200);

        //allow to move:
        allowedToMove = true;
        for (let i = 0; i < directions.length; i++) {
          directions[i].setAttribute("visible", "true");
        }
        dialogueParent.setAttribute("visible", "false");

        //add clickable from pokemons
        for (let i = 0; i < wildPokemons.length; i++) {
          wildPokemons[i].classList.add("clickable");
        }

        battle_music.currentTime = 0;
        battle_music.pause();
        route_music.play();
      }

    }, 2000)

  }


}
