window.onload = () =>{
  const resetPokemon = document.getElementById("js--box");
  const pokemonPicture = document.getElementsByClassName("js--pokemon-picture");
  const pokemonName = document.getElementsByClassName("js--pokemon-name");
  const BASE_URL = "https://pokeapi.co/api/v2/pokemon/";


  resetPokemon.onmouseenter = (event) =>{
    getPokemon();

    resetPokemon.setAttribute("color", "red");
  }

  resetPokemon.onmouseleave = (event) =>{
    resetPokemon.setAttribute("color", "green");
  }


  const getPokemon = () =>{
    for (let i = 0; i < pokemonName.length; i++) {
      let numberOfPokemon = Math.floor(Math.random() * 151 + 1);


      fetch(BASE_URL + numberOfPokemon)
      .then( (data) =>{
        return data.json();
      })
      .then( (response) =>{
        pokemonName[i].setAttribute("value", response.name);
        pokemonPicture[i].setAttribute("src", response.sprites.front_default);
      });
    }
  }
}
